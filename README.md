# Dotfiles

MY own dotfiles (Arch Linux)

![Desktop](https://gitlab.com/bezodnia/dotfiles/-/raw/main/.gitlab/desktop.png)

[Harmony theme](https://github.com/yeyushengfan258/Harmony-kde)

KDE settings:

- Kate theme inspired by Dark+ theme VSCode [Theme](https://github.com/microsoft/vscode/blob/main/extensions/theme-defaults/themes/dark_plus.json)

- Plasmoids: menu11

- Application style: Kvantum Harmony

- Plasma style: Harmony

- Colors: Harmony

- Window decorations: Harmony

- Icons: Tela Circle Pink Dark + Papirus

- Cursors: Vimix

- Splash screen: Breeze

- Sddm theme: McMojave
