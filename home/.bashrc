#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

[ -r /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

# Aliases

alias ls='ls --color=auto'
alias la='ls -a'
alias ll='ls -l'
alias lal='ls -al'
alias d='dirs'

alias grep='grep --color=auto'

alias g='git'

alias ..='cd ..'
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'

alias home='cd $HOME'
alias repos='cd $HOME/repos'
alias dots='cd $HOME/repos/dotfiles'

alias upd='sudo pacman -Syu'
alias unlock='sudo rm /var/lib/pacman/db.lck'
alias cleanup='sudo pacman -Rns $(pacman -Qtdq)'

alias cls='clear'
alias h='history'

alias please='sudo'
alias fucking='sudo'

alias sayonara='shutdown now'
alias stahp='shutdown now'

alias untar='tr -zxvf'

alias status='echo $?'

# Exports

export VISUAL="nvim"
export EDITOR="$VISUAL"

export LANG="en_US.UTF-8"
export LC_ALL="en_US.UTF-8"

export PATH="$PATH:$HOME/repos/dotfiles/scripts"

# Prompt Shell
# PS1='[\u@\h \W]\$ '

# status code
function __nonzero_return() {
    RETVAL=$?
    [ $RETVAL -ne 0 ] && echo "($RETVAL)"
}

# get current status of git repo
function __parse_git_dirty {
	status=`git status 2>&1 | tee`
	dirty=`echo -n "${status}" 2> /dev/null | grep "modified:" &> /dev/null; echo "$?"`
	untracked=`echo -n "${status}" 2> /dev/null | grep "Untracked files" &> /dev/null; echo "$?"`
	ahead=`echo -n "${status}" 2> /dev/null | grep "Your branch is ahead of" &> /dev/null; echo "$?"`
	newfile=`echo -n "${status}" 2> /dev/null | grep "new file:" &> /dev/null; echo "$?"`
	renamed=`echo -n "${status}" 2> /dev/null | grep "renamed:" &> /dev/null; echo "$?"`
	deleted=`echo -n "${status}" 2> /dev/null | grep "deleted:" &> /dev/null; echo "$?"`
	bits=''
	if [ "${renamed}" == "0" ]; then
		bits=">${bits}"
	fi
	if [ "${ahead}" == "0" ]; then
		bits="*${bits}"
	fi
	if [ "${newfile}" == "0" ]; then
		bits="+${bits}"
	fi
	if [ "${untracked}" == "0" ]; then
		bits="?${bits}"
	fi
	if [ "${deleted}" == "0" ]; then
		bits="x${bits}"
	fi
	if [ "${dirty}" == "0" ]; then
		bits="!${bits}"
	fi
	if [ ! "${bits}" == "" ]; then
		echo " ${bits}"
	else
		echo ""
	fi
}

# get current branch in git repo
function __parse_git_branch() {
	BRANCH=`git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'`
	if [ ! "${BRANCH}" == "" ]
	then
		STAT=`__parse_git_dirty`
		
		# if [ $STAT == "!" ]; then
		# 	echo -e " \e[33m(${BRANCH})\e[m"
		# elif [ $STAT == "?" ]; then
		# 	echo -e " \e[32m($BRANCH)\e[m"
			echo -e " \e[32m(${BRANCH}${STAT})\e[m"
		# fi
	else
		echo ""
	fi
}

PS1="\[\e[m\]\[\e[34m\]\[\e[34m\][\[\e[m\]\[\e[34m\]\u\[\e[m\]\[\e[34m\]@\[\e[m\]\[\e[34m\]\h\[\e[m\] \[\e[34m\]\W\[\e[m\]\[\e[34m\]]\[\e[m\]\[\e[m\]\`__parse_git_branch\` \[\e[34m\]\$\[\e[m\] "

export VK_ICD_FILENAMES="/usr/share/vulkan/icd.d/radeon_icd.i686.json:/usr/share/vulkan/icd.d/radeon_icd.x86_64.json"
export AMD_VULKAN_ICD="RADV"
. "$HOME/.cargo/env"

if [[ $(ps --no-header --pid=$PPID --format=cmd) != "zsh" ]]
then
	exec zsh
fi
