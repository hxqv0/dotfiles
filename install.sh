#!/usr/bin/bash

[ -f $HOME/.bashrc ] && { rm .bashrc; ln -s home/.bashrc $HOME/.bashrc; }

ln -s home/.fonts.conf $HOME/.fonts.conf
ln -s home/.gdbinit $HOME/.gdbinit
ln -s home/.gitconfig $HOME/.gitconfig
ln -s home/.vimrc $HOME/.vimrc

[ ! -f $HOME/.vim/plugged ] &&  { curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim }

[ ! -f $HOME/.config/fontconfig ] && { cp -r config/fontconfig $HOME/.config/fontconfig }