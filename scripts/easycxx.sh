#!/usr/bin/env bash

export rootDir="$HOME/repos/dotfiles/scripts/easycxx"

setup() {
    if [[ $1 == "" ]]; then
        echo "Can't create project. Please, take a name project!"
        exit 1
    fi

    echo "Create C++ Project..."
    local path="$HOME/repos/cpp/$1"
    mkdir $path
    cp -rf ${rootDir}/* $path
    cp ${rootDir}/.gitignore $path
    cp -r ${rootDir}/.vscode $path
    mv "$path/.vscode/lib.code-workspace" "$path/.vscode/$1.code-workspace"
    mv "$path/test/src/lib_tests.cpp" "$path/test/src/$1_tests.cpp"
    sed -i "s/PROJECT_NAME/$1/" "$path/CMakeLists.txt"
    sed -i "s/PROJECT_NAME_tests/$1_tests/" "$path/test/CMakeLists.txt"
    sed -i "s/PROJECT_NAME_tests.cpp/$1_tests.cpp/" "$path/test/CMakeLists.txt"
    sed -i "s/PROJECT_NAME/$1/" "$path/test/CMakeLists.txt"
}

main() {
    setup $1
    cd "$HOME/repos/cpp/$1"
    git init
    code .vscode/$1.code-workspace
}

(($_s_)) || main $1