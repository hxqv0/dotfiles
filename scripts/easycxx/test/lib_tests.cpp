#include <boost/ut.hpp>

constexpr auto sum = [](auto... args) { return (0 + ... + args); };

int main() {
  using namespace boost::ut;

  "hello world"_test = [] {
    expect(0_i == sum());
    expect(1_i == sum(1));
    expect(3_i == sum(1, 2));
  };
}